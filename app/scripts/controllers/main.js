'use strict';

angular.module('angularTranslateApp')
  .controller('MainCtrl', ['$scope', function ($scope) {

    $scope.languages = [
      {name:'Chineese', code:0},
      {name:'German', code:1},
      {name:'French', code:2},
    ];

    $scope.translate = function (langCode) {

         switch(langCode)
         {
         case 0:
           $scope.translatedText = "helloChineese";
           break;
         case 1:
           $scope.translatedText = "helloGerman";
           break;
	     case 2:
	       $scope.translatedText = "helloFrench";
	       break;           
         default:
           $scope.translatedText = "No translation selected";
         }
     }

  }]);
